"""
@file mcp9808,py
@brief mcp9808 driver

Created on Tue Feb  2 13:10:16 2021

@author: Michelle Chandler
"""

from pyb import I2C
import array

class mcp9808:
    '''
    @brief 
    @details 
    '''   
    
    def __init__(self, i2c):
        '''
        @brief 
        @param       
        '''
        ##
        self.i2c_obj = i2c
        
    def check(self, addr):
        '''
        @brief 
        @param       
        '''
        found_addr = self.i2c_obj.scan()
        if found_addr != []:
            for n in range(len(found_addr)):
                if found_addr[n] == addr:   # will addr be set to 00110001?
                    print('found')
                else:
                    print('not found')
        
        
    def celsius(self):
        '''
        @brief 
        @param       
        '''
        # OR:
        # read data from sensor memory
        # register = 00000101
        data = array.array ('H', (0 for index in range (1)))
        temp_bytes = self.i2c.mem_read(data, addr=0x18, 0x05, timeout=1000)
        # pg 25 in mcp9808 datasheet for unit conversion
        temp_c = 
        return temp_c
        
    def farenheight(self):
        '''
        @brief 
        @param       
        '''
        # will be similar to the celcius method in terms of asking for data
        # will need to convert from celcius to faren
        data = array.array ('H', (0 for index in range (1)))
        temp_bytes = self.i2c.mem_read(data, addr=0x18, 0x05, timeout=1000)
        temp_f = temp_c * 1.8 + 32
        return temp_f
        
if __name__ == "__main__":
    
 