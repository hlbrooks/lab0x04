# -*- coding: utf-8 -*-
"""
@file me405lab4_main.py
@brief
@date Created on Tue Feb  2 14:26:16 2021
@author: Michelle Chandler
"""

# main.py
# open csv file
# i2c objects - used with driver to get ambient temp
# adcall object - used to get mcu temp
# try except blocks 
# - try : everything except closing csv
# - except: close/save csv

# 1. set up objects
# 2. open csv file
# 3. query temp sensor/ mcu temp
# - mcu_temp = adcall.read_core_temp()
# - mcp9808.celsius(mcp) 
# - generate timestamp
# 4. save temp and time to csv file
# 5. mcu_temp =  None / 
# 6. sleep(60000)

amb_temp = []
mcu_temp = []
time     = []

import pyb
from pyb import I2C
from mcp9808 import mcp9808
import utime

## Master i2c object
# this is the nucleo I think?
print('i2c object')
i2c = I2C(1, I2C.MASTER)  # create and init as master
# adcall object
print('adcall object')
adcall = pyb.ADCAll(12, 0x70000)  # specify 12 bit resolution and mask value specifies just internal channels
                                    # preprocessed MCU data is accessed on ADC channel 16
current_time = 0

while True: 
    try:
        print('reading core temp')
        mcu_temp_var = adcall.read_core_temp()
        print('mcu_temp_var: ' + str(mcu_temp_var))
        mcu_temp.append(mcu_temp_var)
#        print('mcu_temp' + str(mcu_temp))
        amb_temp_var = mcp9808.celsius()
        amb_temp.append(amb_temp_var)
        current_time = current_time + 1  # current time in seconds
        time.append(current_time)
        mcu_temp_var = None
        amb_temp_var = None
        utime.sleep_ms(60000)
        print('sleep')

    except KeyboardInterrupt: 
        # these lines enough to open and close file
        with open ("me405lab4.csv", "w") as me405lab4:
            # how much will write into file
            # for line in range (10) 
            for n in range(len(mcu_temp)):
                # what to write into file
                # {:s} = string. will not be using :s
                # me405lab4.write ('mcu_temp: {:}\r\n'.format(mcu_temp[n]))
                me405lab4.write ('Time [s]: {:}, STM32 temp: {:}, Ambient temp: {:}\r\n'.format(time[n], mcu_temp[n], amb_temp[n]))
                # ... 
        print ("The file has by now automatically been closed.")
        break 

