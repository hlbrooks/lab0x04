# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 11:10:24 2021

@author: Hunter
"""

# ampy --port COM5 get me405lab4.csv me405lab4.cs

# main.py
# open csv file
# i2c objects - used with driver to get ambient temp
# adcall object - used to get mcu temp
# try except blocks 
# - try : everything except closing csv
# - except: close/save csv

# 1. set up objects
# 2. open csv file
# 3. query temp sensor/ mcu temp
# - mcu_temp = adcall.read_core_temp()
# - mcp9808.celsius(mcp) 
# 4. save temp to csv file
# 5. mcu_temp =  None / 
# 6. sleep(60000)
amb_temp = []
mcu_temp = []
time     = []

import pyb
import utime
import micropython

micropython.alloc_emergency_exception_buf(200)  # the buffer contains 200 bytes of memory

print('adcall object')
adcall = pyb.ADCAll(12, 0x70000)

time_to_measure = utime.ticks_ms()    # 8 hours is 28800s or 28800000ms
current_time = 0
time_stamp= 0
exit_program = 0

current_time = utime.ticks_ms()

while True: 
    try:
        if (utime.ticks_diff(current_time, time_to_measure) >= 0):
            adcall.read_vref()
            mcu_temp_var = adcall.read_core_temp()
            mcu_temp.append(mcu_temp_var)
            print('MCU Temp [C]: ' + str(mcu_temp_var))
            time.append(time_stamp)
            print('Time [s]: ' + str(time_stamp))
            time_stamp = time_stamp + 1  # current time in seconds
            time_to_measure += 1000
        else:
            pass

        current_time = utime.ticks_ms()
            
    except KeyboardInterrupt:
        with open ("me405lab4.csv", "w") as me405lab4:
            me405lab4.write ('Time [s]: , mcu_temp [C]:\n')
            # how much will write into file
            for n in range(len(mcu_temp)):
                # what to write into file
                me405lab4.write ('{:}, {:},\n'.format(time[n], mcu_temp[n]))
            print ("The file has by now automatically been closed.")
        break