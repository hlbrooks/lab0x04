# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 19:36:52 2021

@author: Hunter
"""

## This code will be implemented within celsius() to convert to decimal temperature
#  Begin with code loaded within variable "data"

# the output bit layout from the sensor is as follows:
#   [LLLLLLLLAAASUUUU], where bits 'A' to be masked out, 'S' is the sign bit, 'L' is lower byte, 'U' is upper byte
    
# right shift relevant bits from upper byte by 4 bits, left shift lower byte by 4 bits
# adding the results will give total temp in decimal

temp_bytes = 10177      # decimal representation of analog temp

pos_temp = 0            # flag used to test if temp is pos or neg

upper_buf = []          # buffer that will hold upper byte binary
upper_val = 0           # upper byte converted to decimal
lower_buf = []          # buffer that will hold lower byte binary
lower_val = 0           # lower byte converted to decimal

n = 0                   # index used to track current bit being manipulated

if temp_bytes & (1 << 4):    # first test sign of temperature: if 4th bit is 1 neg, else pos
    print('Negative Temperature')
else:
    print('Positive Temperature')
    pos_temp = 1
    
while n < 4:                  # test which bits of upper byte are 1 and store them in buffer
    if temp_bytes & (1 << n):
        upper_buf.append(1)
    else:
        upper_buf.append(0)
    n += 1
i = 0                         # index to hold current location within buffer
while i < 4:                  # calculate the decimal equivalent of upper byte
    upper_val = upper_val + upper_buf[i]*(2**i)
    i += 1

n = 8
i = 0                     
while n < 16:                  # test which bits of upper byte are 1 and store them in buffer
    if temp_bytes & (1 << n):
        lower_buf.append(1)
    else:
        lower_buf.append(0)
    n += 1
    i +=1

i = 0
while i < 8:                 # calculate the decimal equivalent of lower byte
    lower_val = lower_val + lower_buf[i]*(2**i)
    i += 1
    
temp_sum = upper_val*2**4 + lower_val*2**(-4)  # total temp = upper*2^4 + lower*2^(-4)

if pos_temp == 1:
    temp_c = temp_sum           # if pos temp, temp_c = total temp
else:
    temp_c = 256 - temp_sum    # if neg temp, temp_c = 256 - total temp
    
print('The MCP9808 temp in censius is ' + str(temp_c))
