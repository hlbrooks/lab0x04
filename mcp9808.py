"""
@file mcp9808.py
@brief mcp9808 driver to collect temperature sensor data

Created on Tue Feb  2 13:10:16 2021

@author: Michelle Chandler, Hunter Brooks
"""

from pyb import I2C
import array

class mcp9808:
    '''
    @brief    This class is a driver for the mcp9808 temperatures sensor
    @details  This class allows you to read temperatures from the mcp9808 
              in either celcius or farenheight  
    '''   
    
    def __init__(self, i2c):
        '''
        @brief      initializes the mcp9808 driver and import the i2c object for the i2c interface
        @param i2c  i2c objet defined in main .py  
        '''
        ##
        self.i2c = i2c
        
    def check(self, address):
        '''
        @brief          This method checks the available devices on the i2c bus and checks it against the desired device address
        @param address  This parameter is the address being looked for by  using this method
        '''
        found_addr = self.i2c.scan()
        if found_addr != []:
            for n in range(len(found_addr)):
                if found_addr[n] == address:
                    print('found')
                    check = 1
                    return check
                else:
                    print('not found')
                    check = 0
        
    def celsius(self):
        '''
        @brief this method returns the ambient temperature of the room in celsius
        '''
        global temp_c
        
       # OR:
        # read data from sensor memory
        # register = 00000101
        data = array.array ('H', (1 for index in range (1)))
        temp_bytes_ = self.i2c.mem_read(data, 0x18, 5, timeout=1000)
        temp_bytes= int(temp_bytes_[0])
        pos_temp = 0            # flag used to test if temp is pos or neg
        
        upper_buf = []          # buffer that will hold upper byte binary
        upper_val = 0           # upper byte converted to decimal
        lower_buf = []          # buffer that will hold lower byte binary
        lower_val = 0           # lower byte converted to decimal
        
        n = 0                   # index used to track current bit being manipulated
        
        if temp_bytes & (1 << 4):    # first test sign of temperature: if 4th bit is 1 neg, else pos
            print('Negative Temperature')
        else:
            print('Positive Temperature')
            pos_temp = 1
            
        while n < 4:                  # test which bits of upper byte are 1 and store them in buffer
            if temp_bytes & (1 << n):
                upper_buf.append(1)
            else:
                upper_buf.append(0)
            n += 1
        i = 0                         # index to hold current location within buffer
        while i < 4:                  # calculate the decimal equivalent of upper byte
            upper_val = upper_val + upper_buf[i]*(2**i)
            i += 1
        
        n = 8
        i = 0                     
        while n < 16:                  # test which bits of upper byte are 1 and store them in buffer
            if temp_bytes & (1 << n):
                lower_buf.append(1)
            else:
                lower_buf.append(0)
            n += 1
            i +=1
        
        i = 0
        while i < 8:                 # calculate the decimal equivalent of lower byte
            lower_val = lower_val + lower_buf[i]*(2**i)
            i += 1
            
        temp_sum = upper_val*2**4 + lower_val*2**(-4)  # total temp = upper*2^4 + lower*2^(-4)
        
        if pos_temp == 1:
            temp_c = temp_sum           # if pos temp, temp_c = total temp
        else:
            temp_c = 256 - temp_sum    # if neg temp, temp_c = 256 - total temp
            
        
        return temp_c
        
    def farenheight(self):
        '''
        @brief this method returns the ambient temperature of the room in farenheight
        '''
        # will be similar to the celcius method in terms of asking for data
        # will need to convert from celcius to faren
        # data = array.array ('H', (1 for index in range (2)))
        # temp_bytes = self.i2c.mem_read(data, 0x18, 0x05, timeout=1000)
        temp_f = temp_c * 1.8 + 32
        return temp_f