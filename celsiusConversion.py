# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 19:36:52 2021

@author: Hunter
"""

## This code will be implemented within celsius() to convert to decimal temperature
#  Begin with code loaded within variable "data"

# the output bit layout from the sensor is as follows:
#   [AAASMMMMLLLLLLLL], where bits 'A' to be masked out, 'S' is the sign bit, M is MSB, L is LSB

# Objective:
#    mask out upper 3 bits 
#    check sign bit (bit 12) - if 0 positive temp, else negative temp
    
# right shift upper byte by 4 bits, left shift lower byte by 4 bits
# adding the results will give temp in decimal

data_b = 0b11011111000001   # this is for testing purposes (enter in binary value to be converted to temp)
data = int(data_b)          # this is the integer temp (this will hold the data from MCP9808 in our program)
print('The decimal equivalent of entered number is ' + str(data))

pos_temp = 0            # flag used to test if temp is pos or neg

upper_buf = []          # buffer that will hold upper byte binary
upper_val = 0           # upper byte converted to decimal
lower_buf = []          # buffer that will hold lower byte binary
lower_val = 0           # lower byte converted to decimal

n = 0                   # index used to track current bit being manipulated

if data & (1 << 12):    # first test sign of temperature: if bit 12 is 1 neg, else pos
    print('Negative Temperature')
else:
    print('Positive Temperature')
    pos_temp = 1
    
while n < 8:                  # test which bits of lower byte are 1 and store them in buffer
    if data & (1 << n):
        lower_buf.append(1)
    else:
        lower_buf.append(0)
    n += 1

i = 0                         # index for to hold current location within buffer
while i < 8:                 # calculate the decimal equivalent of lower byte
    lower_val = lower_val + lower_buf[i]*(2**i)
    i += 1

i = 0                     
while n < 12:                  # test which bits of upper byte are 1 and store them in buffer
    if data & (1 << n):
        upper_buf.append(1)
    else:
        upper_buf.append(0)
    n += 1
    i +=1
    
i = 0
while i < 4:                 # calculate the decimal equivalent of upper byte
    upper_val = upper_val + upper_buf[i]*(2**i)
    i += 1
    
temp_sum = upper_val*2**4 + lower_val*2**(-4)  # total temp = upper*2^4 + lower*2^(-4)

if pos_temp == 1:
    temp_c = temp_sum           # if pos temp, temp_c = total temp
else:
    temp_c = 256 - temp_sum    # if neg temp, temp_c = 256 - total temp
    
print('The MCP9808 temp in censius is ' + str(temp_c))
